# 1) Escreva um script em Python que substitua o caractere “x” por espaço considerando a
# seguinte frase:
# “Umxpratoxdextrigoxparaxtrêsxtigresxtristes”

# frase = "Umxpratoxdextrigoxparaxtrêsxtigresxtristes"
# print(frase.replace("x", " "))

frase = "Umxpratoxdextrigoxparaxtrêsxtigresxtristes" 
print(frase.replace("x", " "))



# ======================================================================================================
# 2) Escreva um programa que receba o ano de nascimento, e que ele retorne à geração
# a qual a pessoa pertence. Para definir a qual geração uma pessoa pertence temos a
# seguinte tabela:

# Geração        Intervalo

# Baby Boomer -> Até 1964
# Geração X   -> 1965 - 1979
# Geração Y   -> 1980 - 1994
# Geração Z   -> 1995 - Atual

# Para testar se seu script está funcionando, considere os seguintes resultados esperados:

# • ano nascimento = 1988: Geração: Y
# • ano nascimento = 1958: Geração: Baby Boomer
# • ano nascimento = 1996: Geração: Z

# def determinar_geracao(ano_nascimento):
#     if ano_nascimento <= 1964:
#         return "Geração Baby Boomer"
#     elif 1965 <= ano_nascimento <= 1979:
#         return "Geração X"
#     elif 1980 <= ano_nascimento <= 1994:
#         return "Geração Y"
#     else:
#         return "Geração Z"

# def input_ano_nascimento():
#     while True:
#         ano = input("Digite o seu ano de nascimento: ")
#         if ano.isdigit():
#             return int(ano)
#         else:
#             print("Por favor, digite apenas números.")

# ano_nascimento = input_ano_nascimento()
# geracao = determinar_geracao(ano_nascimento)

# print(f"Ano de nascimento = {ano_nascimento}: {geracao}")
























































