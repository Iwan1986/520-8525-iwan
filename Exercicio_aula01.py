# 1) Em muitos programas, nos é solicitado o preenchimento de algumas informações como
# nome, CPF, idade e unidade federativa. Escreva um script em Python que solicite as
# informações cadastrais mencionadas e que em seguida as apresente da seguinte forma:

# -----------------------------
# Confirmação de cadastro:
# Nome: Guido Van Rossum
# CPF: 999.888.777/66
# Idade: 65
# -----------------------------

# nome = input("Informe seu nome:")
# cpf = input("Informe seu CPF:")
# idade = input("Informe sua idade:")

# print("--------------------------")
# print("Confirmacao de cadastro:")
# print(f"Nome: {nome}")
# print(f"CPF: {cpf}")
# print(f"Idade: {idade}")
# print("--------------------------")

#outra forma

# print(f"""
# -------------------------------------
# Cadastro:
# Nome: {nome}
# CPF: {cpf}
# Idade: {idade}
# -------------------------------------""")


# 2) Escreva um script em Python que receba dois números e que seja realizado as seguintes
# operações:
# • soma dos dois números
# • diferença dos dois números
# O resultado deverá ser apresentado conforme a seguir - no exemplo foram digitados os números
# 4 e 2:

# ------------------------------
# Soma: 4 + 2 = 6
# Diferença: 4 - 2 = 2

# print(4+2)
# print(4-2)
# mensagem = "No exemplo foram digitados os numeros 4 e 2"
# print(mensagem)

num1 = int(input("Digite o primeiro numero: "))
num2 = int(input("Digite o segundo numero: "))

soma = num1 + num2
diferenca = num1 - num2

print ("------------------------------")
print ("Soma:", soma)
print ("Diferenca:", diferenca)
print ("------------------------------")


